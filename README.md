<img src="https://gitlab.com/marzuk-zarir/marzuk-zarir/-/raw/master/banner.png" width="100%">

A professional fullstack developer with a passion for building applications. I enjoy working with Markdown, Node.js, React & JamStack. I'm working as independent contractor, you might want to check out my [Linkedin profile](https://linkedin.com/in/marzuk-zarir) & [Portfolio](https://marzuk-zarir.github.io).

<!-- ## 🚀 Latest Blog Posts

Coming Soon -->

## 💬 Get In Touch

- Portfolio site: https://marzuk-zarir.github.io
- Linkedin: https://linkedin.com/in/marzuk-zarir
- Email: business.marzuk-zarir.github.io
